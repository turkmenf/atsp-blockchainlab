import React, {Component} from 'react';
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Bank from "./components/Bank";
import Docs from "./components/Docs";

class App extends Component {
    state = {loading: true, drizzleState: null, currentTab: 0};

    componentDidMount() {
        const {drizzle} = this.props;

        // subscribe to changes in the store
        this.unsubscribe = drizzle.store.subscribe(() => {

            // every time the store updates, grab the state from drizzle
            const drizzleState = drizzle.store.getState();

            // check to see if it's ready, if so, update local component state
            if (drizzleState.drizzleStatus.initialized) {
                this.setState({loading: false, drizzleState });
            }
        });
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    render() {
        if (this.state.loading) return "";
        return (
            <div className="App">
                <Grid container style={{ height: '100%' }}>
                    <Grid item xs={6} >
                        <Paper square elevation={6} style={{ height: '100%', padding: '1rem'}} >
                            <Docs  drizzle={this.props.drizzle}
                                   drizzleState={this.state.drizzleState}
                            />
                        </Paper>
                    </Grid>
                    <Grid item xs={6} style={{ padding: '1.5rem', position: 'fixed', right: 0, width: '100%'}}>
                        <Bank
                          drizzle={this.props.drizzle}
                          drizzleState={this.state.drizzleState}
                        />
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default App;
