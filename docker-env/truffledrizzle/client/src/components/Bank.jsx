import React from "react";
import TextField from '@material-ui/core/TextField';
import { Button, Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

const MULTITUDE_OF = 100000;

export default class Bank extends React.Component {
  state = {
    dataKey: null,
    inputValue: MULTITUDE_OF,
    stackId: null,
    balanceKey: null,
    loanedKey: null,
    interestKey: null,
    lendValue: MULTITUDE_OF,
    withdrawValue: MULTITUDE_OF,
    accountBalance: undefined
  };

  componentDidMount() {
    const { drizzle, drizzleState } = this.props;
    console.log(drizzle);
    console.log(drizzleState);


    const contract = drizzle.contracts.Bank;

    const balanceKey = contract.methods["getBalance"].cacheCall();
    const loanedKey = contract.methods["getLoaned"].cacheCall();
    const interestKey = contract.methods["interestPercentage"].cacheCall();


    this.setState({
      balanceKey,
      loanedKey,
      interestKey,
      accountBalance: drizzleState.accountBalances[drizzleState.accounts[0]]
    });
  }

  componentWillReceiveProps(nextProps) {
    Object.keys(this.props.drizzleState.transactions).forEach((key) => {
      if (this.props.drizzleState.transactions[key].status !== nextProps.drizzleState.transactions[key].status) {
        console.log('updating');
        this.updateBalance();
        return;
      }
    })
  }


  updateBalance = () => {
    const { drizzle, drizzleState } = this.props;
    drizzle.web3.eth.getBalance(drizzleState.accounts[0]).then((balance) => {
      this.setState({
        accountBalance: balance,
      })
    })

  }

  deposit = () => {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.Bank;

    contract.methods["addBalance"].cacheSend({
      from: drizzleState.accounts[0],
      value: this.state.inputValue
    });
    this.updateBalance();

    this.setState({
      inputValue: MULTITUDE_OF,
      accountBalance: this.state.accountBalance - this.state.inputValue
    });
  };

  lend = () => {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.Bank;

    contract.methods["lendFromBank"].cacheSend(this.state.lendValue, {
      from: drizzleState.accounts[0],
    });
    this.updateBalance();


    this.setState({
      lendValue: MULTITUDE_OF,
      accountBalance: this.state.accountBalance + this.state.lendValue
    });
  };

  withdraw = () => {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.Bank;

    contract.methods["withdraw"].cacheSend(this.state.withdrawValue, {
      from: drizzleState.accounts[0],
    });
    this.updateBalance();


    this.setState({
      lendValue: MULTITUDE_OF,
      accountBalance: this.state.accountBalance + this.state.withdrawValue
    });
  };

  render() {
    // get the contract state from drizzleState
    const { Bank } = this.props.drizzleState.contracts;
    // const web3 = this.props.drizzle.web3;
    //
    // // using the saved `dataKey`, get the variable we're interested in
    const balance = Bank.getBalance[this.state.balanceKey];
    const loaned = Bank.getLoaned[this.state.loanedKey];
    const interest = Bank.interestPercentage[this.state.interestKey];

    // if it exists, then we display its value
    return (<div>
      <Grid container spacing={3}>
        <Grid item xs={12} lg={8}>
          <Typography variant="h5" gutterBottom>
            Mock decentralized bank
          </Typography>
          <p>
            <strong>Your balance:</strong><br/> {balance && balance.value}
            <br/>
          </p>
          <p>
            <strong>Your debt:</strong><br/>{loaned && loaned.value}
            <br/>
          </p>
          <p>
            <strong>Interest:</strong><br/> {interest && interest.value}%
            <br/>
          </p>

          <Grid className="pt-4">
            <TextField
              error={this.state.inputValue % MULTITUDE_OF !== 0}
              helperText={`Must be a multitude of ${MULTITUDE_OF}`}
              label="Value"
              variant="outlined"
              style={{ width: '250px' }}
              value={this.state.inputValue}
              onChange={(event) => this.setState({ inputValue: event.target.value })}
            />
            <Button
              color="primary"
              onClick={this.deposit}
              className="ml-3 mt-2"
              disabled={this.state.inputValue % MULTITUDE_OF !== 0}
            >
              Deposit
            </Button>
          </Grid>
          <div className=" pt-5">
            <TextField
              error={this.state.lendValue % MULTITUDE_OF !== 0}
              helperText={`Must be a multitude of ${MULTITUDE_OF}`}
              label="Value"
              variant="outlined"
              style={{ width: '250px' }}

              value={this.state.lendValue}
              onChange={(event) => this.setState({ lendValue: event.target.value })}
            />
            <Button
              color="primary"
              onClick={this.lend}
              className="ml-3 mt-2"
              disabled={this.state.lendValue % MULTITUDE_OF !== 0}
            >
              Lend
            </Button>
          </div>

          <div className="pt-5">
            <TextField
              error={this.state.withdrawValue % MULTITUDE_OF !== 0}
              helperText={`Must be a multitude of ${MULTITUDE_OF}`}
              label="Value"
              style={{ width: '250px' }}
              variant="outlined"
              value={this.state.withdrawValue}
              onChange={(event) => this.setState({ withdrawValue: event.target.value })}
            />
            <Button color="primary" onClick={this.withdraw} className="ml-3 mt-2"
                    disabled={this.state.withdrawValue % MULTITUDE_OF !== 0}>
              Withdraw
            </Button>
          </div>
        </Grid>
        <Grid item xs={12} lg={4} className="pt-5" style={{ borderLeft: '1px solid #eee' }}>
          <strong>Your account:</strong><br/> <code>{this.props.drizzleState.accounts[0]}</code>
          <br/>
          <br/>
          <strong>Balance:</strong><br/> {this.state.accountBalance}
        </Grid>
      </Grid>
    </div>);
  }
}
