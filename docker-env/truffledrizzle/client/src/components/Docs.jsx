import React from "react";
import Typography from "@material-ui/core/Typography";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

const MAX_VALUE = Math.pow(2, 32) - 1;

export default class Docs extends React.Component {
  state = { currentTab: 0 };

  render() {

    return (<>
      <Typography variant="h5" component="h2" gutterBottom>
        Decentralized application with vulnerabilities
      </Typography>
      <p style={{ fontSize: '1rem' }}>
        On the right side you will see a mock up of a decentralized banking application.
        <br/>
        <br/>
        This banking app allows you to open a banking account, deposit Ether and withdraw Ether. The
        bank also keeps track of an interest percentage
        that you have to pay over loans depending on how much ether the banking contract currently
        holds.
        <br/>
        <br/>
        The contract is deployed at address <code>{this.props.drizzle.contracts.Bank.address}</code>
        <br/>
        <br/>
        The application suffers from the following known smart contract vulnerabilities:
      </p>
      <ul>
        <li>
          Reentrancy
        </li>
        <li>
          Fallback
        </li>
        <li>
          DoS Revert
        </li>
        <li>
          Forcibly Sending
        </li>
        <li>
          Integer overflow
        </li>
      </ul>
      You can connect your <a href="http://localhost:3000">Remix IDE</a> to the deployed contract as
      follow:
      <ol style={{ marginTop: '5px' }}>
        <li>In the File Explorer, upload the <code>Bank.sol</code> file from the repository</li>
        <li>Click on the 'Solidity Compiler' tab on the right side</li>
        <li>Press 'Compile Bank.sol'</li>
        <li>Click on the 'Deploy & Run Transactions' tab on the right side</li>
        <li>Select 'Web3 Provider' from the 'Environment' dropdown list (and connect to the default
          address <a href="http://localhost:8545">localhost:8545</a>)
        </li>
        <li>Copy the <code>{this.props.drizzle.contracts.Bank.address}</code> address into the 'At
          Address' field
        </li>
        <li>Press the 'At Address' button. The deployed contract will now appear below the button
          and you will be able to interact with it.
        </li>
      </ol>
      You can now interact with the deployed smart contract through this interface and through
      Remix.
      Note that whenever you execute a certain action, the gas price will be deducted from your
      balance as well.
      Try and exploit the vulnerabilities that are present in the application. The vulnerabilities
      are explained more in debt below.
      <Tabs
        value={this.state.currentTab}
        indicatorColor="primary"
        textColor="primary"
        onChange={(event, newValue) => this.setState({ ...this.state, currentTab: newValue })}
        variant="scrollable"
        scrollButtons="auto"
        className="mt-4"
        aria-label="vulnerabilities tab"
      >
        <Tab label="Reentrancy"/>
        <Tab label="Fallback"/>
        <Tab label="DoS Revert"/>
        <Tab label="Forcible Sending"/>
        <Tab label="Integer overflow"/>
      </Tabs>
      <div className="p-3">
        {this.state.currentTab === 0 && (
          <div>
            <Typography variant="h6" component="h3" gutterBottom>
              Reentrancy Attack
            </Typography>
            <p>
              Re-entrancy is the bug that caused the DAO attack, one of the most famous blockchain
              attacks. Re-entrancy happens in single-thread computing environments, when the
              execution stack
              jumps or calls subroutines, before returning to the original execution.
              Contracts are vulnerable to poor execution ordering.
            </p>

            <p>
              In this application, the withdraw function is vulnerable to this type of attack. By
              repeatedly
              calling the withdraw function before its execution finishes, you will be able to
              deplete the
              contracts funds and send them to your own address.
            </p>
            <p>
            </p>
          </div>
        )}
        {this.state.currentTab === 1 && (
          <div>
            <Typography variant="h6" component="h3" gutterBottom>
              Fallback
            </Typography>
            <p>
              Anyone can call a contract's fallback function. For this reason, there should be no
              advanced logic such as sending Ether in a fallback function. This contract's fallback
              function
              sends ether to anyone who calls it. Try and call this fallback function in Remix.
              You can do this by leaving the 'CALLDATA' field empty and pressing the 'Transact' button.
              You will find that the bank's ether is transferred to your own address.
            </p>
          </div>
        )}{this.state.currentTab === 2 && (
        <div>
          <Typography variant="h6" component="h3" gutterBottom>
            DoS Revert
          </Typography>
          <p>
            A Denial of Service Revert vulnerability can be exploited when a smart contract contains
            logic
            that will attempt to send back ether to a certain address. The contract will check if
            the payment was successful, and if not, it will revert the transaction.
            An adversary can ensure that all payments to their account fail, therefore, blocking the
            program from continuing.
          </p>
          <p>
            Although this smart contract does not contain any particularly interesting logic to
            exploit with this technique.
            It does attempt to transfer funds to account and require it to succeed. You can mock
            this vulnerability by attempting to withdraw money from the bank, but blocking the transaction.
            This will result in your bank balance not being updated at all.
          </p>
        </div>
      )}
        {this.state.currentTab === 3 && (
          <div>
            <Typography variant="h6" component="h3" gutterBottom>
              Forcible sending
            </Typography>
            <p>
              It is possible to forcibly send Ether to a contract without triggering its fallback
              function.
              This is an important consideration when placing important logic in the fallback
              function or making calculations based on a contract's balance.
            </p>
            <p>
              In this banking application, only multitudes of 100000 can be stored and withdrawn.
              For this reason, one might believe that the contract's funds are always a multitude of
              100000 as well.
              This contract contains logic that relies on this assumption to determine the interest
              percentage.
            </p>
            <p>
              You can can forcibly send ether to this contract to keep the contract stuck in its
              current state of interest. If the amount of ether is not divisible by the multitude,
              the corresponding code to update the interest percentage will no longer be executed. This
              is typically done by deploying a second contract that has a <code>selfdestruct</code> method.
              However, in this contract, we have allowed you to achieve the same by using the fallback function.
              You could for example set the wei value of the contract to 123, and subsequently press the 'Transact' button.
              The interest percentage functionality will then be stuck at its currently set value.
            </p>
          </div>
        )}
        {this.state.currentTab === 4 && (
          <div>
            <Typography variant="h6" component="h3" gutterBottom>
              Integer overflow
            </Typography>
            <p>
              The smart contract stores the debt in a <code>uint32</code>, the maximum value that it
              can hold
              is <code>{MAX_VALUE}</code>. You can exploit this vulnerability by repeatedly lending
              from the bank
              until this maximum value is reached and you will see that your debt with the bank
              circles back to 0.
            </p>
          </div>
        )}
      </div>
    </>);
  }
}
