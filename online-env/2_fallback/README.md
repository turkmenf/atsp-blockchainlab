In this task we will learn and experiment with one of the most important types of functions in smart contracts which is Fallback function. 
Various vulnerability occur because of the bad use of fallback functions as you will see shortly.
A contract can have exactly one fallback function. 
It should be unnamed, cannot have arguments, cannot return anything, and has to be external. 
It is executed on a call to the contract if none of the other functions match the given function identifier (or if no data was supplied at all).
The fallback function is also executed whenever the contract receives plain Ether (without data). 
To receive Ether, the fallback function must be marked payable. 
If no payable fallback function exists, the contract cannot receive Ether through regular transactions and throws an exception. 
It is best practice to implement a simple Fallback function if you want your smart contract to generally receive Ether from other contracts and wallets.
A payable fallback function could be as simple as the following:

`function() external payable { }`

The problem is when developers implement key logic inside the fallback function. 
Such bad practices include: changing contract ownership, transferring the funds, etc. inside the fallback function. 
`Fallback.sol` is an example of such bad practice: it demonstrates how you open up your contract to abuse, because anyone can trigger a fallback function. 

Anyone can call a fallback function by:

1. Calling a function that doesn’t exist inside the contract
2. Calling a function without passing in required data
3. Sending Ether without any data to the contract

As you can see in the above contract, there are key logic in the fallback function that allows changing ownership of the contract and therefore stealing its balance.

Make sure you compile and deploy `Fallback.sol` first in the Remix IDE.
Next, figure out a way to invoke the callback function and transfer funds to an account.