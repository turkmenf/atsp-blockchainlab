### DoS with (Unexpected) revert
If an attacker bids using a smart contract which has a fallback function that reverts any payment, the attacker can win any auction. 
When it tries to refund the old leader, it reverts if the refund fails. 
This means that a malicious bidder can become the leader while making sure that any refunds to their address will always fail. 
In this way, they can prevent anyone else from calling the bid() function, and stay the leader forever.

Make sure you compile and deploy `DoS.sol` first in the Remix IDE.
Next, figure out a way to exploit the contract.

source: https://consensys.github.io/smart-contract-best-practices/known_attacks/#dos-with-unexpected-revert