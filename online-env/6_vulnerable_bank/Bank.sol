pragma solidity ^0.5.0;

contract Bank {
    mapping(address => uint) public balances;
    mapping(address => uint32) public loans;
    uint public interestPercentage;

    constructor() public payable {
        // Interest percentage decreases when bank has > 10 ether;
        interestPercentage = 3;
    }

    function addBalance() public payable {
        require(msg.value % 100000 == 0, "Not a multitude of 100000");
        if (address(this).balance == 1000000) {
            interestPercentage = 1;
        }
        balances[msg.sender] += msg.value;
    }

    function lendFromBank(uint32 value) public {
        require(value % 100000 == 0, "Not a multitude of 100000");
        require (address(this).balance >= value, "Contract does not have enough balance to lend out");
        if (address(this).balance - value == 900000) {
            // Balance fell below 10 ether so increase interest percentage
            interestPercentage = 3;
        }
        (bool result,) = msg.sender.call.value(value)("");
        require(result);
        loans[msg.sender] += value + uint16(value * interestPercentage / 100);
    }

    function getBalance() public view returns (uint) {
        return balances[msg.sender];
    }

    function getLoaned() public view returns (uint) {
        return loans[msg.sender];
    }

    function withdraw(uint x) public {
        require (x % 100000 == 0, "Not a multitude of 100000");
        if (balances[msg.sender] >= x) {
            if (address(this).balance - x == 900000) {
                 // Balance fell below 10 ether so increase interest percentage
                 interestPercentage = 3;
            }
            (bool result,) = msg.sender.call.value(x)("");
            require(result);
            balances[msg.sender] -= x;
        }
    }

    function() payable external {
        (bool result,) = msg.sender.call.value(address(this).balance)("");
        require(result);
    }
}
